import React, { Component } from 'react'
import { Containers } from '../Containers/Containers'
import { Container } from '../ComponentTodoList/components/Container'
import { Dropdown } from '../ComponentTodoList/components/Dropdown'
import { Heading1, Heading2, Heading3, Heading4, Heading5 } from '../ComponentTodoList/components/Heading'
import { TextField, Label } from '../ComponentTodoList/components/TextField'
import { Button } from '../ComponentTodoList/components/Button'
import { Table, Tr, Td, Th, Tbody, Thead } from '../ComponentTodoList/components/Table'
import { ThemeProvider } from 'styled-components'
import { DarkThemeTodoList } from '../themes/DarkThemeTodoList'
import { LightThemeTodoList } from '../themes/LightThemeTodoList'
import { PrimaryThemeTodoList } from '../themes/PrimaryThemeTodoList'
import { connect } from 'react-redux'
import { actionAddTask, actionChangeTheme, actionDoneTask, actionDeleteTask, actionEditTask, actionUpdateTask } from '../Redux/Actions/TodoListAtions'
import { arrTheme } from '../themes/ThemeManager'


class TodoList extends Component {

    state = {
        taskName: "",
        disabled: true,
    }

    renderTaskTodo = () => {
        return this.props.taskList.filter(task => !task.done).map((task, index) => {
            return <Tr key={index}>
                <Th style={{ verticalAlign: 'middle' }} className='text-left'>{task.taskName}</Th>
                <Th className='text-right'>
                    <Button onClick={() => {

                        this.setState({
                            disabled: false
                        }, () => { this.props.dispatch(actionEditTask(task)) })
                    }}><i className='fa fa-edit'></i></Button>


                    <Button onClick={() => {
                        this.props.dispatch(actionDoneTask(task.id))
                    }}><i className='fa fa-check'></i></Button>

                    <Button onClick={() => {
                        this.props.dispatch(actionDeleteTask(task.id))
                    }}><i className='fa fa-trash'></i></Button>
                </Th>
            </Tr>
        })
    }
    renderTaskCompleted = () => {
        return this.props.taskList.filter(task => task.done).map((task, index) => {
            return <Tr key={index}>
                <Th style={{ verticalAlign: 'middle' }} className='text-left'>{task.taskName}</Th>
                <Th className='text-right'>
                    <Button onClick={() => {
                        this.props.dispatch(actionDeleteTask(task.id))
                    }}><i className='fa fa-trash'></i></Button>
                </Th>
            </Tr>
        })
    }

    // life cycle phiên bản 16 đã củ nên không sử dụng nó 
    // componentWillReceiveProps(newProps) {
    //     this.setState({
    //         taskName: newProps.taskEdit.taskName
    //     })
    // }

    renderThemes = () => {
        return arrTheme.map((theme, index) => {
            return <option ket={index} value={theme.id}>{theme.name}</option>
        })
    }


    render() {
        return (
            <ThemeProvider theme={this.props.themeTodoList}>
                {/* onchange: Sự kiện xảy ra khi thành phần đã được thay đổi nội dung, 
                giá trị. Đối với ô <input>, <keygen>, <select>, và <textarea>. */}
                <Container className='w-50'>
                    <Dropdown onChange={(e) => {
                        let { value } = e.target
                        // this.props.dispatch({
                        //     type: 'THEME_CHANGE',
                        //     themeId: value
                        // })
                        this.props.dispatch(actionChangeTheme(value))
                    }}>
                        {this.renderThemes()}
                    </Dropdown>
                    <Heading3 className='display-4 text-left'>To Do List</Heading3>
                    <TextField value={this.state.taskName} name="taskName" onChange={(e) => {
                        this.setState({
                            taskName: e.target.value
                        })
                    }} label="Task name" />
                    <Button onClick={() => {
                        // lấy thông tin người dung nhập vào ô input
                        let { taskName } = this.state;

                        // Tạo ra 1 task Project
                        let newTask = {
                            // Dùng Date.now() nó sẽ trả về ngày tháng năm giờ phút giây để id không trùng nhau
                            id: Date.now(),
                            // taskName là giá trị người dùng nhập vào
                            taskName: taskName,
                            done: false
                        }
                        console.log(newTask)
                        this.props.dispatch(actionAddTask(newTask))
                    }} className='ml-2'><i className='fa fa-plus'></i> Add Task</Button>


                    {this.state.disabled ? <Button disabled onClick={() => {
                        this.props.dispatch(actionUpdateTask(this.state.taskName))
                    }} className='ml-2'><i class="fa fa-upload"></i> Update Task</Button>
                        : <Button onClick={() => {
                            let { taskName } = this.state
                            this.setState({
                                disabled: false,
                                taskName: ''
                            }, () => { this.props.dispatch(actionUpdateTask(taskName)) })
                        }} className='ml-2'><i class="fa fa-upload"></i> Update Task</Button>}


                    <hr className='bg-light' />
                    <Heading3 className='text-left'>Task To Do</Heading3>
                    <Table>
                        <Thead>
                            {this.renderTaskTodo()}
                        </Thead>
                    </Table>
                    <Heading3 className='text-left'>Task Comleted</Heading3>
                    <Table>
                        <Thead>
                            {this.renderTaskCompleted()}
                        </Thead>
                    </Table>
                </Container>
            </ThemeProvider >
        )
    }

    // Sử dụng life cycle componentDidUpdate để khi bấm edit thì chỉnh sửa được
    // Đây là life cycle trả về props cũ và state cũ của component trước 
    // khi render ( life cycle này chạy sau render)
    componentDidUpdate(prevProps, prevState) {
        // So sánh nếu như props trước đó (task Edit trước mà khác task Edit sau thì mới setState)

        if (prevProps.taskEdit.id !== this.props.taskEdit.id) {
            this.setState({
                taskName: this.props.taskEdit.taskName
            })
        }

    }
}


const mapStateToProps = state => {
    return {
        themeTodoList: state.ReducerTodoList.themeTodoList,
        taskList: state.ReducerTodoList.taskList,
        taskEdit: state.ReducerTodoList.taskEdit,
    }
}

export default connect(mapStateToProps)(TodoList)