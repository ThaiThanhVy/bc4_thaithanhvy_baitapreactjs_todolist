export const DarkThemeTodoList = {
    bgColor: '#343a40',
    color: '#fff',
    boderButton: '1px solid #fff',
    boderRadiusButton: 'none',
    hoverTextColor: '#343a40',
    hoverBgColor: '#fff',
    borderColor: '#343a40'
}