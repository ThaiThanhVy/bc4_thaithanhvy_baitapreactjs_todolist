// Quản lí tất cả các theme 

import { DarkThemeTodoList } from "./DarkThemeTodoList";
import { LightThemeTodoList } from "./LightThemeTodoList";
import { PrimaryThemeTodoList } from "./PrimaryThemeTodoList";


export const arrTheme = [
    {
        id: 1,
        name: 'Dark Theme',
        theme: DarkThemeTodoList,
    },
    {
        id: 2,
        name: 'Light Theme',
        theme: LightThemeTodoList,
    },
    {
        id: 3,
        name: 'Primary Theme',
        theme: PrimaryThemeTodoList,
    },
]