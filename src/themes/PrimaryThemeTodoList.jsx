export const PrimaryThemeTodoList = {
    bgColor: '#fff',
    color: '#343a40',
    boderButton: '1px solid #343a40',
    boderRadiusButton: 'none',
    hoverTextColor: '#fff',
    hoverBgColor: '#343a40',
    borderColor: '#343a40'
}