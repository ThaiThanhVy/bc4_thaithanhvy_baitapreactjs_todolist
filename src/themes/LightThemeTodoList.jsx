export const LightThemeTodoList = {
    bgColor: '#fff',
    color: '#7952b3',
    boderButton: '1px solid #7952b3',
    boderRadiusButton: 'none',
    hoverTextColor: '#fff',
    hoverBgColor: '#7952b3',
    borderColor: '#7952b3'
}