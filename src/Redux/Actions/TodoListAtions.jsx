import { add_task, change_Theme, done_task, delete_task, edit_task, update_task } from "../../Types/TypesTodoList"


// { return{} } = ({})

export const actionAddTask = (newTask) => {
    return {
        type: add_task,
        newTask
    }
}

export const actionChangeTheme = (themeId) => ({
    type: change_Theme,
    themeId
})

export const actionDoneTask = (taskId) => ({
    type: done_task,
    taskId
})
export const actionDeleteTask = (taskId) => ({
    type: delete_task,
    taskId
})
export const actionEditTask = (task) => ({
    type: edit_task,
    task
})

export const actionUpdateTask = (taskName) => ({
    type: update_task,
    taskName
})
