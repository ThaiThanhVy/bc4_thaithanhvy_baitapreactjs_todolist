import { DarkThemeTodoList } from "../../themes/DarkThemeTodoList";
import { add_task, change_Theme, delete_task, done_task, edit_task, update_task } from "../../Types/TypesTodoList";
import { arrTheme } from "../../themes/ThemeManager";

const stateDefaut = {
    themeTodoList: DarkThemeTodoList,
    taskList: [{
        id: 'task 1', taskName: 'task 1', done: true
    },
    {
        id: 'task 2', taskName: 'task 2', done: false
    },
    {
        id: 'task 3', taskName: 'task 3', done: true
    },
    {
        id: 'task 4', taskName: 'task 4', done: false
    },
    ],

    taskEdit: {
        id: '-1', taskName: '', done: true
    },


}

const ReducerTodoList = (state = stateDefaut, action) => {
    switch (action.type) {
        case add_task: {
            // Kiểm tra rỗng
            if (action.newTask.taskName.trim() === '') {
                alert('Ban Chua Nhap Viec Can Lam')
                return { ...state }
            }
            // kiểm tra tồn tại nếu mà tên công việc giống nhau thì in ra chữ 
            let taskListUpdate = [...state.taskList]
            let index = taskListUpdate.findIndex(
                task => task.taskName === action.newTask.taskName
            )
            if (index !== -1) {
                alert('Task Nay Da Duoc Ghi')
                return { ...state }
            }
            taskListUpdate.push(action.newTask)

            state.taskList = taskListUpdate
            return { ...state }
        }

        case change_Theme: {
            // Tìm theme dựa trên action.themeId được chọn
            let themeUpdate = arrTheme.find(theme => theme.id == action.themeId)
            if (themeUpdate) {
                // Xét lại theme cho state.themeTodoList
                // Lưu ý : những number , string cơ bản thì nó sẽ tự động load lại
                // Còn đối với dữ liệu array và Object muốn dữ liệu thay đổi phải ...
                state.themeTodoList = { ...themeUpdate.theme }
            }
            return { ...state }
        }
        case done_task: {
            let taskListUpdate = [...state.taskList];
            let index = taskListUpdate.findIndex(
                task => task.id === action.taskId
            )
            if (index !== -1) {
                taskListUpdate[index].done = true
            }

            // console.log('done task', action)
            // state.taskList = taskListUpdate
            return { ...state, taskList: taskListUpdate }

        }
        case delete_task: {
            // let taskListUpdate = [...state.taskList];
            // let index = taskListUpdate.findIndex(
            //     task => task.id === action.taskId
            // )
            // if (index !== -1) {
            //     taskListUpdate.splice(index, 1)
            // }
            // // console.log(action)
            // return { ...state, taskList: taskListUpdate }

            // Cach 2
            let taskListUpdate = [...state.taskList];
            // Lấy nó gán cho chính nó nhưng filter không có thằng có taskId đó
            taskListUpdate = taskListUpdate.filter(
                task => task.id !== action.taskId
            )
            return { ...state, taskList: taskListUpdate }

        }

        case edit_task: {
            let taskEdit = action.task
            return { ...state, taskEdit }
        }
        case update_task: {
            // Chỉnh sửa lại taskName của taskEdit
            // Lấy taskName gán cho taskEdit
            let taskName = action.taskName
            // return { ...state, taskName }
            state.taskEdit = { ...state.taskEdit, taskName }
            // Tìm trong taskList Cập nhật lại taskEdit người dùng update
            let taskListUpdateNew = [...state.taskList]

            let index = taskListUpdateNew.findIndex(task => task.id === state.taskEdit.id)

            if (index !== -1) {
                taskListUpdateNew[index] = state.taskEdit
            }
            state.taskList = taskListUpdateNew

            state.taskEdit = { id: '-1', taskName: '', done: false }
            return { ...state }
        }

        default: return { ...state }
    }
}


export default ReducerTodoList;